<?php

namespace SchLabs\LaravelApiResponse\Classes;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Http\Response;

class ApiResponse extends Response
{

    private $status;
    private $code;
    private $message;
    private $data;


    public function __construct($status, $code, $message, $data, $httpStatus, array $headers = [])
    {
        $this->status = $status;
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;

        parent::__construct(
            compact('status', 'code', 'message', 'data'),
            $httpStatus,
            $headers
        );
    }

    /**
     * Morph the given content into JSON.
     *
     * @param  mixed  $content
     * @return string
     */
    protected function morphToJson($content)
    {
        if ($content instanceof Jsonable) {
            return $content->toJson();
        } elseif ($content instanceof Arrayable) {
            return json_encode($content->toArray());
        }
        //Fix for IsJsonColumn classes
        elseif (
            is_array($content) &&
            array_key_exists('data', $content) &&
            is_object($content['data']) &&
            (
                in_array('IsJsonColumn', class_uses($content['data'])) ||
                $content['data'] instanceof Arrayable
            )
        )
        {
            $content['data'] = $content['data']->toArray();
        }


        return json_encode($content);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }



}
