<?php
namespace SchLabs\LaravelApiResponse\Traits;

use SchLabs\LaravelApiResponse\Classes\ApiResponse as CustomResponse;

trait ApiResponse
{
    /**
     * Success response
     *
     * @param array|object $data
     * @param string $message
     * @param int $code
     * @param int $status
     * @return CustomResponse
     */
    protected function success($data = [], string $message = '', int $code = 0, int $status = 200): CustomResponse
    {
        return $this->response($message, true, $code, $status, $data);
    }

    /**
     * Error/failure response
     *
     * @param string|array $message
     * @param int $code
     * @param int $status
     * @param array|object $data
     * @return CustomResponse
     */
    protected function failure($message = "", int $code = 400, int $status = 400, $data = []): CustomResponse
    {
        return $this->response($message, false, $code, $status, $data);
    }

    /**
     * Api response
     *
     * @param string|array $message
     * @param bool $status
     * @param int $code
     * @param int $httpStatus
     * @param array|object $data
     * @return CustomResponse
     */
    protected function response($message, bool $status, int $code, int $httpStatus, $data = []): CustomResponse
    {
        return new CustomResponse(
            $status,
            $code,
            $message,
            $data,
            $httpStatus
        );
    }
}
