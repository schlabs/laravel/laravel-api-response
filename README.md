# laravel-api-response

## Description
Schlabs standard response for api projects

## Installation

Add the following code to the "repositories" key in your composer.json
````
"repositories": {
    ...
    {
        "type": "vcs",
        "url": "https://gitlab.com/schlabs/laravel/laravel-api-response"
    }
    ...
}
````
Run **composer update**

## Usage
* Include the trait "ApiResponse" in your class
* return the following functions
  * $this->success()
  * $this->failure()
